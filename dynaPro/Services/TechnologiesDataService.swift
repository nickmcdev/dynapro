//
//  TechnologiesDataService.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/2/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

class TechnologiesDataService {
    static let instance = TechnologiesDataService()
    
    private let app = [
        Technologies(title: "Java", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp1.png?alt=media&token=09011521-b006-4681-8cf9-748342e752c4"),
        Technologies(title: "Tomcat", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp2.png?alt=media&token=267a7f79-f802-4685-9a39-ba26ab92b105"),
        Technologies(title: "JBoss", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp3.png?alt=media&token=b6bc1e07-2ab3-4efa-bb19-9c7024b0d123"),
        Technologies(title: "GlassFish", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp4.png?alt=media&token=bdf33a33-1d50-4c34-86bb-287e9e590075"),
        Technologies(title: "Grails", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp5.png?alt=media&token=b71d2e9d-d046-4043-91a2-e7708847ec73"),
        Technologies(title: "IBM WebSphere", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp6.png?alt=media&token=48c0a0a4-0e51-468d-966c-d040f72c95f9"),
        Technologies(title: "Jetty", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp7.png?alt=media&token=dbf00c1b-afdc-4166-8594-c7e0c2c4f970"),
        Technologies(title: "TomEE", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp8.png?alt=media&token=6359be60-a703-43b6-984f-62fc67234c2b"),
        Technologies(title: "WildFly", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp9.png?alt=media&token=97deda2d-aec8-4cf8-bf4f-6d06c0032d48"),
        Technologies(title: "WebLogic", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp10.png?alt=media&token=6f83a78d-3f74-48d2-9dc6-201be5c0e73f"),
        Technologies(title: "Play Framework", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp11.png?alt=media&token=f0e719ea-ee67-483b-a6cf-f7ba84dd6e0a"),
        Technologies(title: "Netty", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp12.png?alt=media&token=d5b65c2f-39df-4065-85f7-933714adbc7b"),
        Technologies(title: "Scala", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp13.png?alt=media&token=8eadb46b-4bb9-4a07-b8ec-27caa407bb27"),
        Technologies(title: "Akka", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp14.png?alt=media&token=dd3b000d-d68d-48e2-94d4-b403da4b4b45"),
        Technologies(title: "Spring", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp15.png?alt=media&token=cee14b8e-3d0d-47f2-9f06-f4710674745f"),
        Technologies(title: "Node.js", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp16.png?alt=media&token=7c7a49e7-52fd-4f7e-a46b-73f4c0bf74a7"),
        Technologies(title: ".NET", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp17.png?alt=media&token=2f06e08e-f86e-440f-aa16-c96f1607c13c"),
        Technologies(title: "PHP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp18.png?alt=media&token=47dd1183-c146-42d6-a6da-6c754ff2da0d"),
        Technologies(title: "Golang", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp19.png?alt=media&token=b426feb7-aa53-48d0-9111-c8f67d9e3dd6"),
        Technologies(title: "Apache HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp20.png?alt=media&token=0ae008b3-e4df-44cb-a85e-dd0eafe4ed09"),
        Technologies(title: "IBM HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp21.png?alt=media&token=de5020b1-11ff-421e-9cec-cf2c21f89bd6"),
        Technologies(title: "Microsoft IIS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp22.png?alt=media&token=61a6189c-0f75-435a-96fa-7da7c3f786a4"),
        Technologies(title: "Nginx", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp23.png?alt=media&token=ece74b4f-654c-4d85-b86a-65df098ecd7b"),
        Technologies(title: "Oracle HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp24.png?alt=media&token=f112c8b6-b664-4915-85ff-5611450cb41e"),
        Technologies(title: "Varnish", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp25.png?alt=media&token=b08a7a13-005d-4798-974e-59a4a6c51706"),
        Technologies(title: "Solr", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp26.png?alt=media&token=a76de908-096a-4116-876b-ef944cd01598"),
        Technologies(title: "Elasticsearch", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp27.png?alt=media&token=2a1df667-a95b-4771-9718-87461a2164d0"),
        Technologies(title: "Python", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp28.png?alt=media&token=c90f61cf-8f42-425d-a539-1e08789b7971"),
        Technologies(title: "Perl", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp29.png?alt=media&token=3504a3e0-86ec-47d1-8b5e-73231db8fd51"),
        Technologies(title: "Jython", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp30.png?alt=media&token=e2d3e539-f4ae-4930-b059-df60e6022478"),
        Technologies(title: "JRuby", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp31.png?alt=media&token=9a4ffc90-058d-4677-be7f-2b4fc0f9bfc4"),
        Technologies(title: "Storm", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp32.png?alt=media&token=01e47d90-fcec-48c9-97da-79b2dae15a0f"),
        Technologies(title: "Hadoop", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp33.png?alt=media&token=0c1fabb7-b8c6-4c39-a0a3-5927377e1bb1"),
        Technologies(title: "Spark", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp34.png?alt=media&token=50268408-02a8-4eac-bcac-0cc29687f8ac"),
        Technologies(title: "HAproxy", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp35.png?alt=media&token=100d400e-c073-41c8-acf3-4396103d3a7c"),
        Technologies(title: "Erland", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp36.png?alt=media&token=09fe146b-fa88-48e9-9c9c-91c4ea2b422c"),
        Technologies(title: "Riak", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp37.png?alt=media&token=5037bc3c-a612-4e3e-9017-a7812d4ee734"),
        Technologies(title: "RabbitMQ", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp38.png?alt=media&token=494620d9-cf98-43c7-9dfb-6345b83eed15"),
        Technologies(title: "Ruby", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp39.png?alt=media&token=db629398-6fba-4094-8a84-9152ed686830"),
        Technologies(title: "iOS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp40.png?alt=media&token=4f715b00-c403-408c-af1b-aed71740c15e"),
        Technologies(title: "Android", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp41.png?alt=media&token=67ad1578-5c7b-4dfb-8b56-7bb4f81cf727")
    ]
    
    private let web = [
        Technologies(title: "IE", imageName: "web1.png"),
        Technologies(title: "Edge", imageName: "web2.png"),
        Technologies(title: "Chrome", imageName: "web3.png"),
        Technologies(title: "Firefox", imageName: "web4.png"),
        Technologies(title: "Safari", imageName: "web5.png"),
        Technologies(title: "Opera", imageName: "web6.png"),
        Technologies(title: "iOS Safari", imageName: "web7.png"),
        Technologies(title: "Android Webkit", imageName: "web8.png"),
        Technologies(title: "Cordova", imageName: "web9.png"),
        Technologies(title: "Kony", imageName: "web10.png"),
        Technologies(title: "PhoneGap", imageName: "web11.png"),
        Technologies(title: "JavaScript", imageName: "web12.png"),
        Technologies(title: "jQuery", imageName: "web13.png"),
        Technologies(title: "AngularJS", imageName: "web14.png"),
        Technologies(title: "AMP", imageName: "web15.png"),
        Technologies(title: "React.js", imageName: "web16.png"),
        Technologies(title: "Ember.js", imageName: "web17.png"),
        Technologies(title: "GWT", imageName: "web18.png"),
        Technologies(title: "Ext JS", imageName: "web19.png"),
        Technologies(title: "MooTools", imageName: "web20.png"),
        Technologies(title: "ZK", imageName: "web21.png"),
        Technologies(title: "Dojo", imageName: "web22.png"),
        Technologies(title: "ASP.NET AJAX", imageName: "web23.png"),
        Technologies(title: "ASP.NET Core", imageName: "web24.png"),
        Technologies(title: "prototype", imageName: "web25.png"),
        Technologies(title: "RichFaces", imageName: "web26.png"),
        Technologies(title: "Backbone.js", imageName: "web27.png"),
        Technologies(title: "Sencha Touch", imageName: "web28.png"),
        Technologies(title: "ICEFaces", imageName: "web29.png"),
        Technologies(title: "generic XHR", imageName: "web30.png"),
        Technologies(title: "HTML5", imageName: "web31.png"),
        Technologies(title: "HTTP", imageName: "web32.png"),
        Technologies(title: "HTTP2", imageName: "web33.png"),
        Technologies(title: "Apache HTTP", imageName: "web34.png"),
        Technologies(title: "Microsoft IIS", imageName: "web35.png"),
        Technologies(title: "IBM HTTP", imageName: "web36.png"),
        Technologies(title: "Java App", imageName: "web37.png"),
        Technologies(title: "Oracle HTTP", imageName: "web38.png"),
        Technologies(title: "Node.js", imageName: "web39.png"),
        Technologies(title: "Varnish", imageName: "web40.png"),
        Technologies(title: "WordPress", imageName: "web41.png"),
        Technologies(title: "Drupal", imageName: "web42.png"),
        Technologies(title: "Facebook", imageName: "web43.png"),
        Technologies(title: "Google", imageName: "web44.png"),
        Technologies(title: "Twitter", imageName: "web45.png"),
        Technologies(title: "Google Ads", imageName: "web46.png"),
        Technologies(title: "Amazon Associates", imageName: "web47.png"),
        Technologies(title: "Akamai", imageName: "web48.png"),
        Technologies(title: "CloudFlare", imageName: "web49.png"),
        Technologies(title: "Google Host", imageName: "web50.png"),
        Technologies(title: "jQuery CDN", imageName: "web51.png"),
        Technologies(title: "Ajax", imageName: "web52.png"),
        Technologies(title: "+1000 others", imageName: "web53.png")
    ]
    
    private let cloud = [
        Technologies(title: "AWS", imageName: "cloud1.png"),
        Technologies(title: "Azure", imageName: "cloud2.png"),
        Technologies(title: "Bluemix", imageName: "cloud3.png"),
        Technologies(title: "IBM Cloud", imageName: "cloud4.png"),
        Technologies(title: "Cloud Foundry", imageName: "cloud5.png"),
        Technologies(title: "Docker", imageName: "cloud6.png"),
        Technologies(title: "DC/OS", imageName: "cloud7.png"),
        Technologies(title: "Google Cloud Platform", imageName: "cloud8.png"),
        Technologies(title: "Kubernetes", imageName: "cloud9.png"),
        Technologies(title: "Mesos", imageName: "cloud10.png"),
        Technologies(title: "Netflix OSS", imageName: "cloud11.png"),
        Technologies(title: "OpenShift", imageName: "cloud12.png"),
        Technologies(title: "OpenStack", imageName: "cloud13.png"),
        Technologies(title: "Oracle Cloud", imageName: "cloud14.png"),
        Technologies(title: "Pivotal CF", imageName: "cloud15.png"),
        Technologies(title: "SAP Cloud", imageName: "cloud16.png"),
        Technologies(title: "VMWare Cloud AWS", imageName: "cloud17.png"),
        Technologies(title: "Eureka", imageName: "cloud18.png"),
        Technologies(title: "Hystrix", imageName: "cloud19.png"),
        Technologies(title: "Hyper-V", imageName: "cloud20.png"),
        Technologies(title: "KVM", imageName: "cloud21.png"),
        Technologies(title: "VMWare", imageName: "cloud22.png"),
        Technologies(title: "Xen", imageName: "cloud23.png"),
        Technologies(title: "Windows", imageName: "cloud24.png"),
        Technologies(title: "Linux", imageName: "cloud25.png"),
        Technologies(title: "Ubuntu", imageName: "cloud26.png"),
        Technologies(title: "Red Hat", imageName: "cloud27.png"),
        Technologies(title: "SUSE", imageName: "cloud28.png"),
        Technologies(title: "openSUSE", imageName: "cloud29.png"),
        Technologies(title: "Fedora", imageName: "cloud30.png"),
        Technologies(title: "CoreOS", imageName: "cloud31.png"),
        Technologies(title: "Amazon Linux", imageName: "cloud32.png"),
        Technologies(title: "CentOS", imageName: "cloud33.png"),
        Technologies(title: "Debian", imageName: "cloud34.png"),
        Technologies(title: "Oracle Solaris", imageName: "cloud35.png"),
        Technologies(title: "IBM AIX", imageName: "cloud36.png"),
        Technologies(title: "IBM z/Linux", imageName: "cloud37.png"),
        Technologies(title: "IBM z/OS", imageName: "cloud38.png")
    ]
    
    private let aws = [
        Technologies(title: "EC2", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws1.png?alt=media&token=19f7bb3d-0fb9-4efd-adcb-063f78940709"),
        Technologies(title: "EBS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws2.png?alt=media&token=ac9963aa-6eeb-4732-aad2-d08e51fb0051"),
        Technologies(title: "S3", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws2.png?alt=media&token=ac9963aa-6eeb-4732-aad2-d08e51fb0051"),
        Technologies(title: "RDS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws4.png?alt=media&token=1b023de1-497a-41c5-bdf3-b83e3e1ab8a2"),
        Technologies(title: "DynamoDB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws5.png?alt=media&token=40676dc6-7c69-49c8-b9e2-91b9393da4fa"),
        Technologies(title: "ELB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws6.png?alt=media&token=664f4942-047e-4d53-bd27-1dc4f0b7aac7"),
        Technologies(title: "Lambda", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws7.png?alt=media&token=f674357d-81b3-44d5-8bcd-825e2bf84746"),
        Technologies(title: "CodePipeline", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws8.png?alt=media&token=77fee655-e9fc-42f3-86a8-c605c5d3df49")
    ]
    
    let enterprise = [
        Technologies(title: "IBM Commerce", imageName: "enter1.png"),
        Technologies(title: "IBM WebSphere Portal", imageName: "enter2.png"),
        Technologies(title: "IBM Integration Bus", imageName: "enter3.png"),
        Technologies(title: "IBM DataPower", imageName: "enter4.png"),
        Technologies(title: "IBM DB2", imageName: "enter5.png"),
        Technologies(title: "Magento", imageName: "enter6.png"),
        Technologies(title: "Sitecore", imageName: "enter7.png"),
        Technologies(title: "SAP", imageName: "enter8.png"),
        Technologies(title: "SAP Hybris", imageName: "enter9.png"),
        Technologies(title: "SharePoint", imageName: "enter10.png"),
        Technologies(title: "Citrix", imageName: "enter11.png"),
        Technologies(title: "Demandware", imageName: "enter12.png"),
        Technologies(title: "Salesforce Commerce Cloud", imageName: "enter13.png"),
        Technologies(title: "Exchange Server", imageName: "enter14.png"),
        Technologies(title: "IBM", imageName: "enter15.png"),
        Technologies(title: "LDAP", imageName: "enter16.png"),
        Technologies(title: "Oracle eBusiness", imageName: "enter17.png"),
        Technologies(title: "Oracle PeopleSoft", imageName: "enter18.png"),
        Technologies(title: "Oracle Siebel", imageName: "enter19.png"),
        Technologies(title: "RestSoap", imageName: "enter20.png"),
        Technologies(title: "SMB-CIFS", imageName: "enter21.png"),
        Technologies(title: "SoapUI", imageName: "enter22.png"),
        Technologies(title: "Sybase", imageName: "enter23.png"),
        Technologies(title: "TIBCO ActiveMatrix", imageName: "enter24.png"),
        Technologies(title: "TIBCO BusinessWorks", imageName: "enter25.png"),
        Technologies(title: "TIBCO EMS", imageName: "enter26.png"),
        Technologies(title: "TIBCO Rendezvous", imageName: "enter27.png"),
        Technologies(title: "TLS", imageName: "enter28.png"),
        Technologies(title: "W3C", imageName: "enter29.png"),
        Technologies(title: "Xcode", imageName: "enter30.png"),
        Technologies(title: "XML Soap", imageName: "enter31.png"),
        Technologies(title: "IBM z/OS IMS 14", imageName: "enter32.png"),
        Technologies(title: "IBM z/OS CICS", imageName: "enter33.png"),
        Technologies(title: "IBM z/Linuz Java", imageName: "enter34.png"),
        Technologies(title: "IBM WebSphere MB", imageName: "enter35.png"),
        Technologies(title: "IBM MQ", imageName: "enter36.png"),
        Technologies(title: "CICS TG", imageName: "enter37.png"),
        Technologies(title: "CICS Transaction", imageName: "enter38.png"),
        Technologies(title: "CICS DLI", imageName: "enter39.png"),
        Technologies(title: "CICS MQ Bridge", imageName: "enter40.png"),
        Technologies(title: "IBM IMS SOAP", imageName: "enter41.png"),
        Technologies(title: "IBM IMS TM", imageName: "enter41.png")
    ]
    
    private let database = [
        Technologies(title: "Oracle DB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata1.png?alt=media&token=4c2e796b-5c12-4baa-98d4-163db4c85680"),
        Technologies(title: "SQL Server", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata2.png?alt=media&token=f0c8d645-ab63-405a-add3-6b743e8db006"),
        Technologies(title: "MySQL", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata3.png?alt=media&token=d9c959c9-78a5-4a06-9a81-fd93fbe49d70"),
        Technologies(title: "PostgreSQL", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata4.png?alt=media&token=22de5630-c133-44b8-bf19-6638e01f7de2"),
        Technologies(title: "MongoDB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata5.png?alt=media&token=f8ab7d8e-7022-4bce-8010-bc104d5620d4"),
        Technologies(title: "Memcached", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata6.png?alt=media&token=79f8e5fe-f57d-40fc-8aad-3a4a82f1b402"),
        Technologies(title: "Redis", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata7.png?alt=media&token=b68efeeb-7670-4c28-9fb4-c39bd518ab51"),
        Technologies(title: "Cassandra", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata8.png?alt=media&token=3e4376d3-173f-48c6-b23c-054e945f3ad3"),
        Technologies(title: "Couchbase", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata9.png?alt=media&token=a341314f-7398-4357-a835-74f77cc92445"),
        Technologies(title: "CouchDB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata10.png?alt=media&token=dc2d5420-b29f-48d1-a4dd-de312159e813")
    ]
    
    private let techCategory = [Technologies]()
    
    func getTech(forTechTitle title: String) -> [Technologies] {
        switch title {
        case "Application performance":
            return getApp()
        case "AWS technologies":
            return getAWS()
        case "Cloud & infrastructure":
            return getCloud()
        case "Database technologies":
            return getDatabase()
        case "Enterprise technologies":
            return getEnterprise()
        default:
            return getWeb()
        }
    }
    
    func getApp() -> [Technologies] {
        return app
    }
    
    func getAWS() -> [Technologies] {
        return aws
    }
    
    func getCloud() -> [Technologies] {
        return cloud
    }
    
    func getDatabase() -> [Technologies] {
        return database
    }
    
    func getEnterprise() -> [Technologies] {
        return enterprise
    }
    
    func getWeb() -> [Technologies] {
        return web
    }
}
