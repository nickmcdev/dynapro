//
//  DataService.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/27/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseCore

let DB_BASE = Database.database().reference()


class TestimonialsDataService {
    
    static let instance = TestimonialsDataService()
    
    private var _REF_BASE = DB_BASE
    private var _REF_YOUTUBE = DB_BASE.child("items")
    
    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }
    
    var REF_YOUTUBE: DatabaseReference {
        return _REF_YOUTUBE
    }
    
    
    func getYouTubeData(handler: @escaping (_ testimonials: [Testimonial]) -> ()) {
        var testimonialArray = [Testimonial]()
        REF_YOUTUBE.observeSingleEvent(of: .value) { (youTubeSnapshot) in
            guard let youTubeSnapshot = youTubeSnapshot.children.allObjects as? [DataSnapshot] else { return }
            
            for testimonial in youTubeSnapshot {
                let id = testimonial.childSnapshot(forPath: "id").value as! String
                let title = testimonial.childSnapshot(forPath: "title").value as! String
                let thumbnail = testimonial.childSnapshot(forPath: "url").value as! String
                let testimonial = Testimonial(id: id, title: title, imageName: thumbnail)
                testimonialArray.append(testimonial)
            }
            handler(testimonialArray)
        }
    }
}


