//
//  PlatformsDataService.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/2/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

class PlatformsDataService {
    static let instance = PlatformsDataService()
    
    // For PlatformsVC
    private let platforms = [
        Platforms(title: "Business and performance analytics", imageName: "platform1.png"),
        Platforms(title: "Digital experience monitoring", imageName: "platform2.png"),
        Platforms(title: "Application monitoring", imageName: "platform3.png"),
        Platforms(title: "Cloud, container, and infrastructure", imageName: "platforms4.png")
    ]
    
    func getPlatforms() -> [Platforms] {
        return platforms
    }
    
    // For PlatformDescVC
    private let platformsBusiness = [
        PlatformsDesc(title: "Business insights", desc: "Gain access to critical business-health data in real-time and in one place. Dynatrace is your one-stop shop for understanding the performance of key business transactions and the effects of customer experience on revenue."),
        PlatformsDesc(title: "User behavior analytics", desc: "Find out how successfully your latest features are adopted by your customers and know if you’ve optimized the right entry points. Dynatrace provides facts to accelerate your business with highly accurate real-time insights."),
        PlatformsDesc(title: "Business transaction monitoring", desc: "Dynatrace monitors every single business transaction end-to-end, with no gaps or blind spots. It shows the execution of each individual request as it travels through your technology stack."),
        PlatformsDesc(title: "Application topology discovery", desc: "Dynatrace continuously auto-detects dependencies within your entire application stack. It visualizes your web application architecture end-to-end in real-time."),
        PlatformsDesc(title: "Root cause analysis", desc: "Dynatrace detects anomalies before they affect your customers. Find out where and why applications break in seconds.")
    ]
    
    private let platformsDigital = [
        PlatformsDesc(title: "Real user monitoring", desc: "Dynatrace monitors the activity of all your mobile and web application users, across all devices and browsers, analyzing the data in real-time to assess user satisfaction."),
        PlatformsDesc(title: "Synthetic monitoring", desc: "Measure and compare your mobile and web channels using the world’s best synthetic-monitoring network. Monitor performance from the locations where your customers are located by emulating real user behavior from key geolocations around the world."),
        PlatformsDesc(title: "Mobile app monitoring", desc: "Dynatrace mobile solutions deliver real-time insights to help you optimize each digital moment—from each customer swipe and click all the way to your back-end services."),
        PlatformsDesc(title: "SaaS Vendor Real User Monitoring (RUM)", desc: "Guarantee intelligent deployment of your SaaS solutions based on real-time information about exactly how happy your users are. See how your employees use your software, including real-time status of their experience."),
        PlatformsDesc(title: "User monitoring for corporate applications", desc: "Manage end-user experience for web and non-web enterprise applications including SAP, Oracle EBS and Siebel, Citrix, and Microsoft Exchange. Dynatrace analyzes transaction performance across a wide range of enterprise applications and services, including custom solutions."),
        PlatformsDesc(title: "Digital Experience Insights", desc: "Get detailed analysis with actionable Insights that guide you to take the best actions that improve your digital business. Digital Experience Insights provide this as well as day-to-day management of synthetic and real-user monitoring technologies, daily triage, expertise on-demand, and best-practice measurement design and management.")
    ]
    
    private let platformsAppMon = [
        PlatformsDesc(title: "Deep-dive application monitoring", desc: "Dynatrace provides application performance management with code-level insights for Java, .NET, Node.js, and PHP. Track each transaction, across all tiers, with no gaps or blind spots."),
        PlatformsDesc(title: "Database monitoring", desc: "Dynatrace monitors and analyzes all of your application’s database activities, providing you with visibility all the way down to individual SQL and NoSQL statements."),
        PlatformsDesc(title: "Continuous delivery analytics", desc: "Accelerate innovation with Dynatrace continuous delivery and test automation capabilities. Effectively monitor key architectural metrics with every single build."),
        PlatformsDesc(title: "Mainframe monitoring", desc: "Easily manage complex mainframe applications. See and understand transactions at the mainframe level—out of the box."),
        PlatformsDesc(title: "Wire data analytics", desc: "Dynatrace uses wire data to understand your entire enterprise landscape—applications, clients, servers, and network—providing you with performance and availability insights based on real user experience."),
        PlatformsDesc(title: "Application performance monitoring", desc: "Application performance monitoring enables the detection and diagnosis of availability and performance problems across your entire stack."),
        PlatformsDesc(title: "Service backtrace", desc: "Automatically analyze service call sequences from back-end performance to front-end impact and understand how requests or database statements impact customers.")
        ]
    
    private let platformsCloud = [
        PlatformsDesc(title: "Server monitoring", desc: "Dynatrace brings your entire data center into view to help you with capacity management. Whether you’re monitoring physical servers or cluster nodes, Dynatrace shows you CPU, memory, and network health metrics all the way down to the process level of each Linux and Windows host."),
        PlatformsDesc(title: "Network monitoring", desc: "Dynatrace network monitoring ensures high-quality process communication, revealing the quality of all process connections on your network—including processes distributed across virtualized cloud environments and data centers."),
        PlatformsDesc(title: "Virtualization monitoring", desc: "See the full picture of your dynamic virtualized environments. Dynatrace connects the dots between the dependencies of the vCenters in your data center, the processes that run on them, and your applications."),
        PlatformsDesc(title: "Microservices & container monitoring", desc: "Monitor containerized applications in dynamic environments out-of-the-box. Dynatrace automatically discovers all microservices running in your container environment. See a real-time view of all the connections between your containerized processes, across all hosts and cloud instances."),
        PlatformsDesc(title: "Cloud monitoring", desc: "Dynatrace monitors your entire stack, including private, public, and hybrid clouds. Whether you run on AWS, Azure, Cloud Foundry, or OpenStack, Dynatrace auto-detects all virtualized components and keeps up with all changes."),
        PlatformsDesc(title: "Log analytics", desc: "With Dynatrace log analytics, you gain direct access to the log content of all your system’s mission-critical processes. It’s easy to search for specific log messages that you’re interested in. You can even analyze multiple log files simultaneously—even when log files are stored across multiple hosts."),
        ]
    
    func getPlatforms(forPlatformsTitle title: String) -> [PlatformsDesc] {
        switch title {
        case "Business and performance analytics":
            return getPlatformsBusiness()
        case "Digital experience monitoring":
            return getPlatformsDigital()
        case "Application monitoring":
            return getPlatformsAppMon()
        default:
            return getPlatformsCloud()
        }
    }
    
    
    func getPlatformsBusiness() -> [PlatformsDesc] {
        return platformsBusiness
    }
    
    func getPlatformsDigital() -> [PlatformsDesc] {
        return platformsDigital
    }
    
    func getPlatformsAppMon() -> [PlatformsDesc] {
        return platformsAppMon
    }
    
    func getPlatformsCloud() -> [PlatformsDesc] {
        return platformsCloud
    }
    
    
}
