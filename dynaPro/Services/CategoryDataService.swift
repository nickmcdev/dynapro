//
//  CategoryDataService.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/2/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

class CategoryDataService {
    static let instance = CategoryDataService()
    
//    var appImageArray = [Technologies]()
//    var webImageArray = [Technologies]()
    
    
    // For CategoriesTechnologiesVC
    private let categories = [
        CategoryTechnologies(title: "Application performance", imageName: "technologiesbg1.png"),
        CategoryTechnologies(title: "DevOps & CI/CD", imageName: "technologiesbg2.png"),
        CategoryTechnologies(title: "Web technologies", imageName: "technologiesbg3.png"),
        CategoryTechnologies(title: "Cloud & infrastructure", imageName: "technologiesbg4.png"),
        CategoryTechnologies(title: "AWS technologies", imageName: "technologiesbg5.png"),
        CategoryTechnologies(title: "Enterprise technologies", imageName: "technologiesbg6.png"),
        CategoryTechnologies(title: "Database technologies", imageName: "technologiesbg7.png"),
        CategoryTechnologies(title: "Messaging & alerting integration", imageName: "technologiesbg8.png")
    ]
    
    func getCategories() -> [CategoryTechnologies] {
        return categories
    }
    
    private let app = [
        Technologies(title: "Java", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp1.png?alt=media&token=09011521-b006-4681-8cf9-748342e752c4"),
        Technologies(title: "Tomcat", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp2.png?alt=media&token=267a7f79-f802-4685-9a39-ba26ab92b105"),
        Technologies(title: "JBoss", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp3.png?alt=media&token=b6bc1e07-2ab3-4efa-bb19-9c7024b0d123"),
        Technologies(title: "GlassFish", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp4.png?alt=media&token=bdf33a33-1d50-4c34-86bb-287e9e590075"),
        Technologies(title: "Grails", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp5.png?alt=media&token=b71d2e9d-d046-4043-91a2-e7708847ec73"),
        Technologies(title: "IBM WebSphere", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp6.png?alt=media&token=48c0a0a4-0e51-468d-966c-d040f72c95f9"),
        Technologies(title: "Jetty", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp7.png?alt=media&token=dbf00c1b-afdc-4166-8594-c7e0c2c4f970"),
        Technologies(title: "TomEE", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp8.png?alt=media&token=6359be60-a703-43b6-984f-62fc67234c2b"),
        Technologies(title: "WildFly", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp9.png?alt=media&token=97deda2d-aec8-4cf8-bf4f-6d06c0032d48"),
        Technologies(title: "WebLogic", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp10.png?alt=media&token=6f83a78d-3f74-48d2-9dc6-201be5c0e73f"),
        Technologies(title: "Play Framework", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp11.png?alt=media&token=f0e719ea-ee67-483b-a6cf-f7ba84dd6e0a"),
        Technologies(title: "Netty", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp12.png?alt=media&token=d5b65c2f-39df-4065-85f7-933714adbc7b"),
        Technologies(title: "Scala", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp13.png?alt=media&token=8eadb46b-4bb9-4a07-b8ec-27caa407bb27"),
        Technologies(title: "Akka", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp14.png?alt=media&token=dd3b000d-d68d-48e2-94d4-b403da4b4b45"),
        Technologies(title: "Spring", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp15.png?alt=media&token=cee14b8e-3d0d-47f2-9f06-f4710674745f"),
        Technologies(title: "Node.js", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp16.png?alt=media&token=7c7a49e7-52fd-4f7e-a46b-73f4c0bf74a7"),
        Technologies(title: ".NET", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp17.png?alt=media&token=2f06e08e-f86e-440f-aa16-c96f1607c13c"),
        Technologies(title: "PHP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp18.png?alt=media&token=47dd1183-c146-42d6-a6da-6c754ff2da0d"),
        Technologies(title: "Golang", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp19.png?alt=media&token=b426feb7-aa53-48d0-9111-c8f67d9e3dd6"),
        Technologies(title: "Apache HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp20.png?alt=media&token=0ae008b3-e4df-44cb-a85e-dd0eafe4ed09"),
        Technologies(title: "IBM HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp21.png?alt=media&token=de5020b1-11ff-421e-9cec-cf2c21f89bd6"),
        Technologies(title: "Microsoft IIS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp22.png?alt=media&token=61a6189c-0f75-435a-96fa-7da7c3f786a4"),
        Technologies(title: "Nginx", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp23.png?alt=media&token=ece74b4f-654c-4d85-b86a-65df098ecd7b"),
        Technologies(title: "Oracle HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp24.png?alt=media&token=f112c8b6-b664-4915-85ff-5611450cb41e"),
        Technologies(title: "Varnish", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp25.png?alt=media&token=b08a7a13-005d-4798-974e-59a4a6c51706"),
        Technologies(title: "Solr", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp26.png?alt=media&token=a76de908-096a-4116-876b-ef944cd01598"),
        Technologies(title: "Elasticsearch", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp27.png?alt=media&token=2a1df667-a95b-4771-9718-87461a2164d0"),
        Technologies(title: "Python", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp28.png?alt=media&token=c90f61cf-8f42-425d-a539-1e08789b7971"),
        Technologies(title: "Perl", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp29.png?alt=media&token=3504a3e0-86ec-47d1-8b5e-73231db8fd51"),
        Technologies(title: "Jython", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp30.png?alt=media&token=e2d3e539-f4ae-4930-b059-df60e6022478"),
        Technologies(title: "JRuby", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp31.png?alt=media&token=9a4ffc90-058d-4677-be7f-2b4fc0f9bfc4"),
        Technologies(title: "Storm", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp32.png?alt=media&token=01e47d90-fcec-48c9-97da-79b2dae15a0f"),
        Technologies(title: "Hadoop", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp33.png?alt=media&token=0c1fabb7-b8c6-4c39-a0a3-5927377e1bb1"),
        Technologies(title: "Spark", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp34.png?alt=media&token=50268408-02a8-4eac-bcac-0cc29687f8ac"),
        Technologies(title: "HAproxy", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp35.png?alt=media&token=100d400e-c073-41c8-acf3-4396103d3a7c"),
        Technologies(title: "Erland", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp36.png?alt=media&token=09fe146b-fa88-48e9-9c9c-91c4ea2b422c"),
        Technologies(title: "Riak", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp37.png?alt=media&token=5037bc3c-a612-4e3e-9017-a7812d4ee734"),
        Technologies(title: "RabbitMQ", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp38.png?alt=media&token=494620d9-cf98-43c7-9dfb-6345b83eed15"),
        Technologies(title: "Ruby", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp39.png?alt=media&token=db629398-6fba-4094-8a84-9152ed686830"),
        Technologies(title: "iOS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp40.png?alt=media&token=4f715b00-c403-408c-af1b-aed71740c15e"),
        Technologies(title: "Android", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fapp41.png?alt=media&token=67ad1578-5c7b-4dfb-8b56-7bb4f81cf727")
    ]
    
    private let devops = [
        Technologies(title: "Ansible", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops1.png?alt=media&token=46ba93b0-1199-4931-92bc-ec0dfbffad3c"),
        Technologies(title: "Apache Ant", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops2.png?alt=media&token=7ff3cba3-3298-4108-82f8-3d2f0929d381"),
        Technologies(title: "Apache JMeter", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops3.png?alt=media&token=53c6b127-5475-444b-ab9e-3108abfda752"),
        Technologies(title: "Bamboo", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops4.png?alt=media&token=e92a53c9-3a4f-400d-a4bf-8ab9760fc2ba"),
        Technologies(title: "BlazeMeter", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops5.png?alt=media&token=790ead9c-77f8-4f42-ba88-fa362c84d015"),
        Technologies(title: "Chef", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops6.png?alt=media&token=fff719f5-301a-4b00-8e67-a1c9a3beea0c"),
        Technologies(title: "Concourse", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops7.png?alt=media&token=a88a73da-577f-40e9-a936-e30faef9b423"),
        Technologies(title: "Deis", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops8.png?alt=media&token=93b849b1-1599-4d1d-aee9-870f0a1164e3"),
        Technologies(title: "Electric Cloud", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops9.png?alt=media&token=48205f71-831a-4fa5-9696-b6c3e2521471"),
        Technologies(title: "Eclipse", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops10.png?alt=media&token=910d6813-40bd-40d9-b746-2c7b9e4250e0"),
        Technologies(title: "Gatling", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops11.png?alt=media&token=88e317cf-e642-4460-9ea6-d8c799d31ef1"),
        Technologies(title: "Git", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops12.png?alt=media&token=411e3e0c-9bdf-423a-aa2b-e347b6af90e7"),
        Technologies(title: "GitHub", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops13.png?alt=media&token=b1c3fd92-18e2-4fe9-aa8d-26fa58ef306e"),
        Technologies(title: "Gradle", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops14.png?alt=media&token=33b83496-e416-4bbf-9616-e78927e50f85"),
        Technologies(title: "IntelliJ", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops15.png?alt=media&token=90ce6796-c1a8-43a0-a37e-dada3f87a5b2"),
        Technologies(title: "Jenkins", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops16.png?alt=media&token=e7e16621-5559-4286-9a54-ee717fd40454"),
        Technologies(title: "Jira", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops17.png?alt=media&token=ab813259-269e-4601-9911-798078b00b17"),
        Technologies(title: "JUnit", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops18.png?alt=media&token=a940402b-04ab-441f-9630-a74619c50822"),
        Technologies(title: "LoadRunner", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops19.png?alt=media&token=28b024ef-3a0e-480a-9021-b43793ee76e9"),
        Technologies(title: "Maven", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops20.png?alt=media&token=4ae6ca1b-bd90-42cb-83be-68ea647477ea"),
        Technologies(title: "MS Build", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops21.png?alt=media&token=33c4db09-2ab6-4088-96b0-a0d057ad7951"),
        Technologies(title: "MSTest", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops22.png?alt=media&token=c20214ef-de3d-4582-bd0f-4ac8b76543e1"),
        Technologies(title: "Nant", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops23.png?alt=media&token=9afbd371-e56f-457d-80a2-d471744928c2"),
        Technologies(title: "NUnit", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops24.png?alt=media&token=c9f0a629-9091-43e0-b7fb-f249a462a4e6"),
        Technologies(title: "Puppet", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops25.png?alt=media&token=66e89d39-da7a-4f30-8747-d7c580edf3d9"),
        Technologies(title: "SaltStack", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops26.png?alt=media&token=36cba3b2-b386-448f-a4f7-08114c077e83"),
        Technologies(title: "Selenium", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops27.png?alt=media&token=02d5ab31-aabe-45b8-9d60-0547a4c007e3"),
        Technologies(title: "Subversion", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops28.png?alt=media&token=c4ce5186-aac0-4263-9c5f-d018e26a1f56"),
        Technologies(title: "TeamCity", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops29.png?alt=media&token=01e512ad-094a-4f77-9bc2-847e309cbc3d"),
        Technologies(title: "Visual Studio", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops30.png?alt=media&token=6ee675c3-fd0f-42bb-9658-f9257af42750"),
        Technologies(title: "Wercker", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdevops31.png?alt=media&token=45fc4e25-b272-43da-afe7-9fbee753915d")
    ]
    
    private let web = [
        Technologies(title: "IE", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb1.png?alt=media&token=159c3b22-bbfb-4a7f-b6e2-aeaad116c26f"),
        Technologies(title: "Edge", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb2.png?alt=media&token=dd396d29-c4fd-46ed-90cb-f59cb0be220d"),
        Technologies(title: "Chrome", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb3.png?alt=media&token=38a3c644-b81b-42e8-9ba0-02678b37b12b"),
        Technologies(title: "Firefox", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb4.png?alt=media&token=faacdebe-bd10-4325-a930-3e5d2bb35dc1"),
        Technologies(title: "Safari", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb5.png?alt=media&token=312b469f-e806-421f-bfae-abf78d112865"),
        Technologies(title: "Opera", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb6.png?alt=media&token=36cca617-ca0e-4569-9750-743e2fc7fefb"),
        Technologies(title: "iOS Safari", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb7.png?alt=media&token=992effca-2550-4b86-a9ca-45ba58dc8c55"),
        Technologies(title: "Android Webkit", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb8.png?alt=media&token=0b802caf-2caf-44e1-9c62-2b51934eed10"),
        Technologies(title: "Cordova", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb9.png?alt=media&token=60000538-40e0-44d4-b51e-249496c56989"),
        Technologies(title: "Kony", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb10.png?alt=media&token=1034e639-1cbb-46c9-8234-a75b03a786d0"),
        Technologies(title: "PhoneGap", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb11.png?alt=media&token=0d4eff20-6f3c-4b6f-8f74-c067c5c25471"),
        Technologies(title: "JavaScript", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb12.png?alt=media&token=17ce8424-b2ba-4a02-ac27-adfaeda38ff2"),
        Technologies(title: "jQuery", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb13.png?alt=media&token=95a63d03-9780-4cba-be26-5436009b349c"),
        Technologies(title: "AngularJS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb14.png?alt=media&token=801fb20c-427e-405d-93cf-d2d94bf94481"),
        Technologies(title: "AMP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb15.png?alt=media&token=a916a33a-1687-4b2c-b5f6-1515351283d5"),
        Technologies(title: "React.js", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb16.png?alt=media&token=269c7c25-8eaa-447f-a0c4-a3e02d7d0f38"),
        Technologies(title: "Ember.js", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb17.png?alt=media&token=e014e688-92b2-453d-9e59-310e337cc130"),
        Technologies(title: "GWT", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb18.png?alt=media&token=4e9f7387-1097-47a8-b358-8db6fc197655"),
        Technologies(title: "Ext JS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb19.png?alt=media&token=5a8089b7-0e3b-4c80-a28b-2c431afee1e5"),
        Technologies(title: "MooTools", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb20.png?alt=media&token=c315d3ff-5ea0-45ae-8ed2-76ce40e9a9f8"),
        Technologies(title: "ZK", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb21.png?alt=media&token=dc27821d-d0a9-4b0a-8159-e0da418d860c"),
        Technologies(title: "Dojo", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb22.png?alt=media&token=c0ab0894-1053-4f0b-824d-3a7e307d81d9"),
        Technologies(title: "ASP.NET AJAX", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb23.png?alt=media&token=759caef3-a2d7-4cd1-bcb2-7a57bd342915"),
        Technologies(title: "ASP.NET Core", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb24.png?alt=media&token=d9780a9f-ddad-435c-beac-370c016e2612"),
        Technologies(title: "prototype", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb25.png?alt=media&token=36f13ec6-b8e9-4619-b05e-37a2dc09127d"),
        Technologies(title: "RichFaces", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb26.png?alt=media&token=605ea20b-9e93-42a5-80b5-c917ed422adf"),
        Technologies(title: "Backbone.js", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb27.png?alt=media&token=c71dc6db-a025-4922-bd85-9a14e6a77d41"),
        Technologies(title: "Sencha Touch", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb28.png?alt=media&token=d3cfe195-b035-47c4-9059-83231191e48e"),
        Technologies(title: "ICEFaces", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb29.png?alt=media&token=2623db4d-76df-4885-8812-45389dd21359"),
        Technologies(title: "generic XHR", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb30.png?alt=media&token=76a205ce-8dcc-47ca-a447-58f4cfc55d3d"),
        Technologies(title: "HTML5", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb31.png?alt=media&token=6d86c856-417e-43cf-9a24-726394fcc722"),
        Technologies(title: "HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb32.png?alt=media&token=8c5d0c60-60f2-4cc9-965c-25bc6a98c6dc"),
        Technologies(title: "HTTP2", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb33.png?alt=media&token=bb548530-a784-429a-8fe6-2b3c91f3b949"),
        Technologies(title: "Apache HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb34.png?alt=media&token=de82c82e-96d5-40af-b32d-c080a0227642"),
        Technologies(title: "Microsoft IIS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb35.png?alt=media&token=14a6f2c7-c1b4-4424-9b4f-31d8f016fbab"),
        Technologies(title: "IBM HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb36.png?alt=media&token=da095d9d-b8fe-4d33-8c78-81090dba6a5b"),
        Technologies(title: "Java App", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb37.png?alt=media&token=ec04014f-3104-486b-bf23-655fbd104937"),
        Technologies(title: "Oracle HTTP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb38.png?alt=media&token=6c2f106e-c7d4-400b-9f3a-ccd09255243b"),
        Technologies(title: "Node.js", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb39.png?alt=media&token=a0edb542-0818-4b18-9985-ea58bcee34fd"),
        Technologies(title: "Varnish", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb40.png?alt=media&token=6a34d678-dc5e-4ff2-b242-7fa5cc42da69"),
        Technologies(title: "WordPress", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb41.png?alt=media&token=17dcf4d3-bd99-4521-9079-3e6054c45a7d"),
        Technologies(title: "Drupal", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb42.png?alt=media&token=450866a0-c474-4d99-bd41-f3842d9bdc3c"),
        Technologies(title: "Facebook", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb43.png?alt=media&token=09aa4878-06a7-415f-b49d-d65e3cffe88e"),
        Technologies(title: "Google", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb44.png?alt=media&token=56cdd167-7fb1-4001-8505-97400dfbbb73"),
        Technologies(title: "Twitter", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb45.png?alt=media&token=183bc04c-94eb-4b92-b3fe-45cbfcd0d090"),
        Technologies(title: "Google Ads", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb46.png?alt=media&token=5600c78d-7946-46e3-a805-17582c571381"),
        Technologies(title: "Amazon Associates", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb47.png?alt=media&token=f984f29c-2954-488a-a31d-0e19420943f0"),
        Technologies(title: "Akamai", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb48.png?alt=media&token=edcb2a7c-d9f6-4683-a812-579ece11dd13"),
        Technologies(title: "CloudFlare", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb49.png?alt=media&token=2a81f04d-de26-4bde-ba80-9eefa1d9e7e7"),
        Technologies(title: "Google Host", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb50.png?alt=media&token=afa86a58-8dab-430f-9698-14fd92fc4bf3"),
        Technologies(title: "jQuery CDN", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb51.png?alt=media&token=878068d3-2349-4f10-b659-1868a0c989cf"),
        Technologies(title: "Ajax", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb52.png?alt=media&token=d7e3e75b-8d9d-4086-951c-b0dcbe3fc33b"),
        Technologies(title: "+1000 others", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fweb53.png?alt=media&token=ca42347d-b51c-4749-acee-8cac41271c14")
    ]
    
    private let cloud = [
        Technologies(title: "AWS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud1.png?alt=media&token=0cf79e15-8e67-488a-b815-e2d50097247e"),
        Technologies(title: "Azure", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud2.png?alt=media&token=474a1a2b-9f74-4617-8e43-179c04896b91"),
        Technologies(title: "Bluemix", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud3.png?alt=media&token=ce523fe5-083a-49d5-992f-0e7fa1e20eef"),
        Technologies(title: "IBM Cloud", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud4.png?alt=media&token=a4d639fc-fd0c-4922-aaf9-c37b4b4a5f4a"),
        Technologies(title: "Cloud Foundry", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud5.png?alt=media&token=3c144beb-1f87-4d8d-beb0-244b74e005a4"),
        Technologies(title: "Docker", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud6.png?alt=media&token=088d6f86-2b2b-427d-b367-90cbda95f6ab"),
        Technologies(title: "DC/OS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud7.png?alt=media&token=739b7f57-a8d5-4379-8649-05fa76f95490"),
        Technologies(title: "Google Cloud Platform", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud8.png?alt=media&token=d69440b1-28d7-42a6-af44-c7ef5de7c90f"),
        Technologies(title: "Kubernetes", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud9.png?alt=media&token=2de1cf40-e24f-4fea-85d8-61afa8c8afa7"),
        Technologies(title: "Mesos", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud10.png?alt=media&token=ff40ae7a-d632-4714-a07c-c736a23afc97"),
        Technologies(title: "Netflix OSS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud11.png?alt=media&token=6076be35-629f-4300-b54e-f6889275ad39"),
        Technologies(title: "OpenShift", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud12.png?alt=media&token=b2082e7e-2837-497b-bfc7-11d0d0f4f2dc"),
        Technologies(title: "OpenStack", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud13.png?alt=media&token=d7e2f39b-c54a-44c0-b926-6d7edc70107a"),
        Technologies(title: "Oracle Cloud", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud14.png?alt=media&token=3f3b7166-65df-4570-88bb-44851fb2d751"),
        Technologies(title: "Pivotal CF", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud15.png?alt=media&token=4de374d7-029f-432e-bcbf-b2431086647e"),
        Technologies(title: "SAP Cloud", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud16.png?alt=media&token=f3e6fe9a-058c-44aa-b9e3-6d2947e144fc"),
        Technologies(title: "VMWare Cloud AWS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud17.png?alt=media&token=df84ffd3-7b42-4f3e-a498-0a9cf1ca3554"),
        Technologies(title: "Eureka", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud18.png?alt=media&token=b44bfe7c-cb27-4bf4-9a3b-045af1b10b3d"),
        Technologies(title: "Hystrix", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud19.png?alt=media&token=d94298ed-afc8-42b5-b538-637f227049c1"),
        Technologies(title: "Hyper-V", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud20.png?alt=media&token=953303f9-5429-40b7-8593-c007f4c07657"),
        Technologies(title: "KVM", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud21.png?alt=media&token=e404e4c3-8719-4e54-ba7d-cace7ab73d1b"),
        Technologies(title: "VMWare", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud22.png?alt=media&token=d67962d1-58cf-4ef8-8665-a329b00972e5"),
        Technologies(title: "Xen", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud23.png?alt=media&token=8fa382cb-e636-4ceb-8595-dd8a8fec4cfc"),
        Technologies(title: "Windows", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud24.png?alt=media&token=23d3f176-2580-4fa3-9bd6-23d8ba0318b7"),
        Technologies(title: "Linux", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud25.png?alt=media&token=bd688f71-7675-4289-b241-21b030bd3e01"),
        Technologies(title: "Ubuntu", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud26.png?alt=media&token=29ace98a-2d21-4151-8ceb-5788b72d377a"),
        Technologies(title: "Red Hat", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud27.png?alt=media&token=02974e71-5d9d-46f2-9239-406c97e1e594"),
        Technologies(title: "SUSE", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud28.png?alt=media&token=db770d7c-026a-4238-baf4-bf5bf7d73858"),
        Technologies(title: "openSUSE", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud29.png?alt=media&token=4be075d5-9142-4f19-a012-2ef66dd8002b"),
        Technologies(title: "Fedora", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud30.png?alt=media&token=e9fe73bd-4a7c-461d-9bcd-a710dd4d6767"),
        Technologies(title: "CoreOS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud31.png?alt=media&token=52e58b8c-86e7-4b58-bafb-bf0ca683fe94"),
        Technologies(title: "Amazon Linux", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud32.png?alt=media&token=fb42b12f-3b96-4049-b0d7-f29a9b908826"),
        Technologies(title: "CentOS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud33.png?alt=media&token=3ab5016e-5281-4bfb-ab0e-25308360333c"),
        Technologies(title: "Debian", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud34.png?alt=media&token=cd786169-d526-4bf3-8506-5a5d12ab453d"),
        Technologies(title: "Oracle Solaris", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud35.png?alt=media&token=c9bf713d-16a3-40a5-a848-b9adf91e6211"),
        Technologies(title: "IBM AIX", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud36.png?alt=media&token=b285fa8a-2d3f-4af9-81b5-1b81cc64b3a1"),
        Technologies(title: "IBM z/Linux", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud37.png?alt=media&token=5b60b278-17d8-46f3-aa1d-299986193d65"),
        Technologies(title: "IBM z/OS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fcloud38.png?alt=media&token=5c18d786-7311-4722-8503-fea4c3998ce3")
    ]
    
    private let aws = [
        Technologies(title: "EC2", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws1.png?alt=media&token=19f7bb3d-0fb9-4efd-adcb-063f78940709"),
        Technologies(title: "EBS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws2.png?alt=media&token=ac9963aa-6eeb-4732-aad2-d08e51fb0051"),
        Technologies(title: "S3", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws2.png?alt=media&token=ac9963aa-6eeb-4732-aad2-d08e51fb0051"),
        Technologies(title: "RDS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws4.png?alt=media&token=1b023de1-497a-41c5-bdf3-b83e3e1ab8a2"),
        Technologies(title: "DynamoDB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws5.png?alt=media&token=40676dc6-7c69-49c8-b9e2-91b9393da4fa"),
        Technologies(title: "ELB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws6.png?alt=media&token=664f4942-047e-4d53-bd27-1dc4f0b7aac7"),
        Technologies(title: "Lambda", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws7.png?alt=media&token=f674357d-81b3-44d5-8bcd-825e2bf84746"),
        Technologies(title: "CodePipeline", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Faws8.png?alt=media&token=77fee655-e9fc-42f3-86a8-c605c5d3df49")
    ]
    
    let enterprise = [
        Technologies(title: "IBM Commerce", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter1.png?alt=media&token=aaf81999-a70e-43cd-9229-ad4ca3e2c688"),
        Technologies(title: "IBM WebSphere Portal", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter2.png?alt=media&token=c5220908-c46a-4b5d-baef-a61d48eee1b5"),
        Technologies(title: "IBM Integration Bus", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter3.png?alt=media&token=540f817e-964e-4978-a106-1754669e1fc9"),
        Technologies(title: "IBM DataPower", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter4.png?alt=media&token=3742ff2d-ef61-4896-908b-f4e3c5e3f823"),
        Technologies(title: "IBM DB2", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter5.png?alt=media&token=a33fec58-b455-4147-bd12-834d9ca02536"),
        Technologies(title: "Magento", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter6.png?alt=media&token=03a3aa6c-80d6-4b3d-b2d4-cd5f1bea8c0d"),
        Technologies(title: "Sitecore", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter7.png?alt=media&token=dcf793cb-129a-4fb8-a151-ff9b897e368c"),
        Technologies(title: "SAP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter8.png?alt=media&token=5c38554e-3406-466c-88d5-121990848341"),
        Technologies(title: "SAP Hybris", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter9.png?alt=media&token=16c6d9b9-92ff-4853-946e-df9824db8593"),
        Technologies(title: "SharePoint", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter10.png?alt=media&token=f8ed0f2a-7200-4fab-bf0e-e1112e287bd2"),
        Technologies(title: "Citrix", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter11.png?alt=media&token=09d9d496-376b-4540-9b2a-d8cecced101b"),
        Technologies(title: "Demandware", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter12.png?alt=media&token=3db4d162-66c5-4367-abec-9bfb30b46a40"),
        Technologies(title: "Exchange Server", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter13.png?alt=media&token=36b9b799-553f-45bb-8776-edba25227f32"),
        Technologies(title: "IBM", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter14.png?alt=media&token=ca77a5cc-8aa4-4f2d-85e4-427aec66f017"),
        Technologies(title: "LDAP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter15.png?alt=media&token=7edd022a-50c7-4039-8a5a-3c9393966822"),
        Technologies(title: "Oracle eBusiness", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter16.png?alt=media&token=9375f324-0ed0-4e41-8e01-f8b36ec4d915"),
        Technologies(title: "Oracle PeopleSoft", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter17.png?alt=media&token=dbd3ff07-4374-497e-abfe-bb1660df1157"),
        Technologies(title: "Oracle Siebel", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter18.png?alt=media&token=cc0de34b-9986-40d2-9cf7-f80f9879c1ee"),
        Technologies(title: "RestSoap", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter19.png?alt=media&token=b3781188-9eaa-4a0a-a085-7f9f8ae2382f"),
        Technologies(title: "SMB-CIFS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter20.png?alt=media&token=dabaa674-f685-4b84-8ff5-af93f732f82f"),
        Technologies(title: "SoapUI", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter21.png?alt=media&token=4e182b81-9954-46c4-9c90-2e6f6e0e625e"),
        Technologies(title: "Sybase", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter22.png?alt=media&token=7a58050a-1751-402d-b6a7-539895eac593"),
        Technologies(title: "TIBCO ActiveMatrix", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter23.png?alt=media&token=23b2260e-f1e1-47e1-8c78-b6a671c30664"),
        Technologies(title: "TIBCO BusinessWorks", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter24.png?alt=media&token=c3bc8608-5c97-4849-a34a-ed14a8d79d56"),
        Technologies(title: "TIBCO EMS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter25.png?alt=media&token=d8c86062-f3ff-4c1d-800f-dee2fdb0c0f6"),
        Technologies(title: "TIBCO Rendezvous", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter26.png?alt=media&token=a5627bc5-84f9-47db-9e7f-6791682c7e3f"),
        Technologies(title: "TLS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter27.png?alt=media&token=c155e41a-f7f2-40c8-aeb5-69c894c1e4f3"),
        Technologies(title: "W3C", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter28.png?alt=media&token=e1a5662d-9f27-4873-bf71-6ea4cb505c6b"),
        Technologies(title: "Xcode", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter29.png?alt=media&token=953f3178-b0d0-469c-a5fa-d8638ee6549f"),
        Technologies(title: "XML Soap", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter30.png?alt=media&token=c14ac359-7657-433b-b0c2-09305616bf1e"),
        Technologies(title: "IBM z/OS IMS 14", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter31.png?alt=media&token=6b9e7fa0-5d18-4639-ac8b-23f6a0fe6397"),
        Technologies(title: "IBM z/OS CICS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter32.png?alt=media&token=691616ca-fd80-4be8-a959-9386fdd9496a"),
        Technologies(title: "IBM z/Linuz Java", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter33.png?alt=media&token=6fc71d5d-7e31-44ad-9ec9-6bfd11138d7c"),
        Technologies(title: "IBM WebSphere MB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter34.png?alt=media&token=87f75b1f-f88a-4c1b-9c2d-648ec745bddc"),
        Technologies(title: "IBM MQ", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter35.png?alt=media&token=35f6c3ef-3db7-4bc3-ac3f-2bc829771f0d"),
        Technologies(title: "CICS TG", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter36.png?alt=media&token=5106fa7e-1395-4820-90fb-5345895a5b2a"),
        Technologies(title: "CICS Transaction", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter37.png?alt=media&token=bc92ea1b-7ea0-4ee4-b899-a427aad3823e"),
        Technologies(title: "CICS DLI", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter38.png?alt=media&token=23c98f1c-454a-4b16-b733-2ead8c85e217"),
        Technologies(title: "CICS MQ Bridge", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter39.png?alt=media&token=76b9b51f-8502-4519-bfe0-b84967033ae7"),
        Technologies(title: "IBM IMS SOAP", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter40.png?alt=media&token=857b7648-4550-407e-9265-44464e4e6dd1"),
        Technologies(title: "IBM IMS TM", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fenter41.png?alt=media&token=a1b5bef1-3ab5-4db7-be34-ec6927b2e935")
    ]
    
    private let database = [
        Technologies(title: "Oracle DB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata1.png?alt=media&token=4c2e796b-5c12-4baa-98d4-163db4c85680"),
        Technologies(title: "SQL Server", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata2.png?alt=media&token=f0c8d645-ab63-405a-add3-6b743e8db006"),
        Technologies(title: "MySQL", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata3.png?alt=media&token=d9c959c9-78a5-4a06-9a81-fd93fbe49d70"),
        Technologies(title: "PostgreSQL", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata4.png?alt=media&token=22de5630-c133-44b8-bf19-6638e01f7de2"),
        Technologies(title: "MongoDB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata5.png?alt=media&token=f8ab7d8e-7022-4bce-8010-bc104d5620d4"),
        Technologies(title: "Memcached", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata6.png?alt=media&token=79f8e5fe-f57d-40fc-8aad-3a4a82f1b402"),
        Technologies(title: "Redis", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata7.png?alt=media&token=b68efeeb-7670-4c28-9fb4-c39bd518ab51"),
        Technologies(title: "Cassandra", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata8.png?alt=media&token=3e4376d3-173f-48c6-b23c-054e945f3ad3"),
        Technologies(title: "Couchbase", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata9.png?alt=media&token=a341314f-7398-4357-a835-74f77cc92445"),
        Technologies(title: "CouchDB", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fdata10.png?alt=media&token=dc2d5420-b29f-48d1-a4dd-de312159e813")
    ]
    
    private let integration = [
        Technologies(title: "Android", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration1.png?alt=media&token=22d20f1e-d132-4cec-baef-3ccc232e4c78"),
        Technologies(title: "Bitbucket", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration2.png?alt=media&token=922e99f4-4116-441c-8e0a-8e671b9f1696"),
        Technologies(title: "HipChat", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration3.png?alt=media&token=43160e1b-920f-4491-82f3-b57cf0cae98e"),
        Technologies(title: "iOS", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration4.png?alt=media&token=46e6c15f-7337-4662-a415-51b2f80977b8"),
        Technologies(title: "Jira", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration5.png?alt=media&token=7faf291b-e119-4d0c-8ead-d92d61d5e0b9"),
        Technologies(title: "OpsGenie", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration6.png?alt=media&token=369a3409-42bd-460e-bd11-b858ca644bd4"),
        Technologies(title: "PagerDuty", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration7.png?alt=media&token=53372c38-1608-4f18-9a7e-0c5208edcd82"),
        Technologies(title: "ServiceNow", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration8.png?alt=media&token=de8e3ca0-1857-40a2-8527-64bdcfe3a3bf"),
        Technologies(title: "Slack", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration9.png?alt=media&token=81926844-d155-44c2-985b-5097a2d267ff"),
        Technologies(title: "Splunk", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration10.png?alt=media&token=8bde6cf8-16d5-4bd8-9937-8b7e75802638"),
        Technologies(title: "Trello", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration11.png?alt=media&token=ef156ecf-d3d4-44a1-ac75-2e707ece3a22"),
        Technologies(title: "VictorOps", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration12.png?alt=media&token=b6237108-7985-4f11-ae13-ff0d381324bb"),
        Technologies(title: "Webhooks", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration13.png?alt=media&token=020bb76d-b6fa-4189-8900-c3c70043678a"),
        Technologies(title: "Windows", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration14.png?alt=media&token=f40e426b-c343-4403-8caf-bffea0fb27d6"),
        Technologies(title: "xMatters", imageName: "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fintegration15.png?alt=media&token=7c43f2d4-4f57-4b36-9496-3bfb69b26867")
    ]
    
    private let techCategory = [Technologies]()
    
    func getTech(forCategoryTitle title: String) -> [Technologies] {
        switch title {
        case "Application performance":
            return getApp()
        case "DevOps & CI/CD":
            return getDevops()
        case "AWS technologies":
            return getAWS()
        case "Cloud & infrastructure":
            return getCloud()
        case "Database technologies":
            return getDatabase()
        case "Enterprise technologies":
            return getEnterprise()
        case "Messaging & alerting integration":
            return getIntegration()
        default:
            return getWeb()
        }
    }
    
    func getApp() -> [Technologies] {
        return app
    }
    
    func getDevops() -> [Technologies] {
        return devops
    }
    
    func getWeb() -> [Technologies] {
        return web
    }
    
    func getAWS() -> [Technologies] {
        return aws
    }
    
    func getCloud() -> [Technologies] {
        return cloud
    }
    
    func getDatabase() -> [Technologies] {
        return database
    }
    
    func getEnterprise() -> [Technologies] {
        return enterprise
    }
    
    func getIntegration() -> [Technologies] {
        return integration
    }
    
    
    
    func getAppImages() -> [String] {
        var appImages = [String]()
        for apps in app {
            appImages.append(apps.imageName)
        }
        return appImages
    }
    
    func getAppTitles() -> [String] {
        var appTitles = [String]()
        for apps in app {
            appTitles.append(apps.title)
        }
        return appTitles
    }
    
    func getDevopsImages() -> [String] {
        var devopsImages = [String]()
        for devop in devops {
            devopsImages.append(devop.imageName)
        }
        return devopsImages
    }
    
    func getDevopsTitles() -> [String] {
        var devopsTitles = [String]()
        for devop in app {
            devopsTitles.append(devop.title)
        }
        return devopsTitles
    }
    
    func getWebImages() -> [String] {
        var webImages = [String]()
        for webs in web {
            webImages.append(webs.imageName)
        }
        return webImages
    }
    
    func getWebTitles() -> [String] {
        var webTitles = [String]()
        for webs in web {
            webTitles.append(webs.title)
        }
        return webTitles
    }
    
    func getAWSImages() -> [String] {
        var awsImages = [String]()
        for awss in aws {
            awsImages.append(awss.imageName)
        }
        return awsImages
    }
    
    func getAWSTitles() -> [String] {
        var awsTitles = [String]()
        for awss in aws {
            awsTitles.append(awss.title)
        }
        return awsTitles
    }
    
    func getCloudImages() -> [String] {
        var cloudImages = [String]()
        for clouds in cloud {
            cloudImages.append(clouds.imageName)
        }
        return cloudImages
    }
    
    func getCloudTitles() -> [String] {
        var cloudTitles = [String]()
        for clouds in cloud {
            cloudTitles.append(clouds.title)
        }
        return cloudTitles
    }
    
    func getDataImages() -> [String] {
        var dataImages = [String]()
        for datas in database {
            dataImages.append(datas.imageName)
        }
        return dataImages
    }
    
    func getDataTitles() -> [String] {
        var dataTitles = [String]()
        for datas in database {
            dataTitles.append(datas.title)
        }
        return dataTitles
    }
    
    func getEnterImages() -> [String] {
        var enterImages = [String]()
        for enters in enterprise {
            enterImages.append(enters.imageName)
        }
        return enterImages
    }
    
    func getEnterTitles() -> [String] {
        var enterTitles = [String]()
        for enters in enterprise {
            enterTitles.append(enters.title)
        }
        return enterTitles
    }
    
    func getIntegImages() -> [String] {
        var integImages = [String]()
        for integs in integration {
            integImages.append(integs.imageName)
        }
        return integImages
    }
    
    func getIntegTitles() -> [String] {
        var integTitles = [String]()
        for integs in integration {
            integTitles.append(integs.title)
        }
        return integTitles
    }
    
    
    
}
