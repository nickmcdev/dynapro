//
//  PlatformsDesc.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/12/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

struct PlatformsDesc {
    
    private(set) public var title: String
    private(set) public var desc: String
    
    init(title: String, desc: String) {
        self.title = title
        self.desc = desc
    }
}
