//
//  CategoryTechnologies.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/27/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

struct CategoryTechnologies {
    
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}
