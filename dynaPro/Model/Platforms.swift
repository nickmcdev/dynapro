//
//  Platforms.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/28/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

struct Platforms {
    
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    
    }
}
