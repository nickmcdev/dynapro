//
//  Testimonial.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/17/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

struct Testimonial {
    private var _id: String
    private var _title: String
    private var _imageName: String
    
    var id: String {
        return _id
    }
    
    var title: String {
        return _title
    }
    
    var imageName: String {
        return _imageName
    }
    
    init(id: String, title: String, imageName: String) {
        self._id = id
        self._title = title
        self._imageName = imageName
    }
}
