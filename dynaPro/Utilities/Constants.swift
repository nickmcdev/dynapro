//
//  Constants.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/1/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()
typealias Error = NSError
