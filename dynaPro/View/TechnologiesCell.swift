//
//  TechnologiesCell.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/18/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class TechnologiesCell: UICollectionViewCell {
    
    // Outlets
    @IBOutlet weak var techImageView: UIImageView!
    @IBOutlet weak var techLabel: UILabel!

    func configureCell(technologies: Technologies) {
        techImageView.image = UIImage(named: technologies.imageName)
        techLabel.text = technologies.title
    }
}

