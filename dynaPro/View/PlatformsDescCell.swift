//
//  PlatformsDescCell.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/12/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class PlatformsDescCell: UICollectionViewCell {
    
    // Outlets
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    func configureCell(platformsDesc: PlatformsDesc) {
        titleLbl.text = platformsDesc.title
        descLbl.text = platformsDesc.desc
    }
    
}
