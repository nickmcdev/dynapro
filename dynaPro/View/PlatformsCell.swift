//
//  PlatformsCell.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/28/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class PlatformsCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var platformImage: UIImageView!
    @IBOutlet weak var platformTitle: UILabel!
    
    func configureCell(platform: Platforms) {
        platformImage.image = UIImage(named: platform.imageName)
        platformTitle.text = platform.title
        
    }
}
