//
//  TestimonialCell.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/17/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class TestimonialCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoLabel: UILabel!

}
