//
//  CategoryTechnologiesCell.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/27/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class CategoryTechnologiesCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    func configureCell(category: CategoryTechnologies) {
        categoryImage.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    
    }
}
