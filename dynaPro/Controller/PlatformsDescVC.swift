//
//  PlatformsDescVC.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/12/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class PlatformsDescVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    // Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    private(set)var platformsDesc = [PlatformsDesc]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self

        // Remove the "Back" text on Navigation Bar
        self.navigationController?.navigationBar.topItem?.title = " "
        
    }
    
    func initPlatforms(platforms: Platforms) {
        platformsDesc = PlatformsDataService.instance.getPlatforms(forPlatformsTitle: platforms.title)
        navigationItem.title = platforms.title
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return platformsDesc.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "platformsDescCell", for: indexPath) as? PlatformsDescCell {
            let platformDesc = platformsDesc[indexPath.row]
            cell.configureCell(platformsDesc: platformDesc)
            return cell
        }
        return PlatformsDescCell()
    }

}
