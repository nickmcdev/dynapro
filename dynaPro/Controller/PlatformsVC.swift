//
//  PlatformsVC.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/28/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class PlatformsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PlatformsDataService.instance.getPlatforms().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PlatformsCell") as? PlatformsCell {
            let platform = PlatformsDataService.instance.getPlatforms()[indexPath.row]
            cell.configureCell(platform: platform)
            return cell
        } else {
            return PlatformsCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let platform = PlatformsDataService.instance.getPlatforms()[indexPath.row]
        performSegue(withIdentifier: "PlatformsDescVC", sender: platform)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let platformsDescVC = segue.destination as? PlatformsDescVC {
            //            let barBtn = UIBarButtonItem()
            //            barBtn.title = ""
            //            navigationItem.backBarButtonItem = barBtn
            assert(sender as? Platforms != nil)
            platformsDescVC.initPlatforms(platforms: sender as! Platforms)
        }
    }
}
