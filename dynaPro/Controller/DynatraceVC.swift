//
//  FirstViewController.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/11/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit
import SDWebImage


class DynatraceVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    
    
    let managedImageURL = "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fmanaged.png?alt=media&token=d16b3f50-4d69-4890-a450-0772d7887d44"
    
    let managed = ["Gain all the conveniences of Dynatrace SaaS", "Full compliance with your company’s policies.", "Dynatrace Mission Control takes care of operations and updates automatically.", "Store data on your local infrastructure.", "Scale Dynatrace horizontally during run-time."]
    
    let saasImageURL = "https://firebasestorage.googleapis.com/v0/b/dynapro-187519.appspot.com/o/dynaPro%2Fsaas.png?alt=media&token=59f70f57-e348-499f-9747-631a9bcf8f4d"
    
    let saas = ["No infrastructure investments.", "Data is stored securely in the Dynatrace cloud.", "Works out-of-the-box and can be up and running in under 5 minutes.", "We ensure that Dynatrace scales with your applications.", "Dynatrace Mission Control takes care of operations and updates automatically."]

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func segmentControlChange(_ sender: Any) {
        if segmentControl.selectedSegmentIndex == 0 {
            imageView.sd_setImage(with: URL(string: saasImageURL))
            lbl1.text = saas[0]
            lbl2.text = saas[1]
            lbl3.text = saas[2]
            lbl4.text = saas[3]
            lbl5.text = saas[4]
        } else {
            imageView.sd_setImage(with: URL(string: managedImageURL))
            lbl1.text = managed[0]
            lbl2.text = managed[1]
            lbl3.text = managed[2]
            lbl4.text = managed[3]
            lbl5.text = managed[4]
        }
    }
}

