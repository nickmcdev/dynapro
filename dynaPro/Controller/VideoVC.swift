//
//  VideoVC.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/17/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit
import YouTubePlayer


class VideoVC: UIViewController {
    
    var videoID = ""
    
    // Outlets
    @IBOutlet var videoPlayer: YouTubePlayerView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove the "Back" text on Navigation Bar
        self.navigationController?.navigationBar.topItem?.title = " "
        
        videoPlayer.delegate = self
        
        loadVideo()
        
        playerReady(videoPlayer)
        print(videoID)

        
    }
    
}
extension VideoVC: YouTubePlayerDelegate {
    
    func playerReady(_ videoPlayer: YouTubePlayerView) {
        if videoPlayer.ready == true {
            print("Player ready!")
            videoPlayer.play()
        } else {
            print("Player not ready!")
        }
    }
    
    func playerStateChanged(_ videoPlayer: YouTubePlayerView, playerState: YouTubePlayerState) {
        print("Player state changed! \(playerState)")
    }
    
    func playerQualityChanged(_ videoPlayer: YouTubePlayerView, playbackQuality: YouTubePlaybackQuality) {
        print("Player quality changed!")
    }
    
    func loadVideo() {
        videoPlayer.playerVars = [
            "playsinline": "1" as AnyObject,
            "controls": "1" as AnyObject,
            "showinfo": "0" as AnyObject
        ]
        videoPlayer.loadVideoID(videoID)
    }
        
    
}
