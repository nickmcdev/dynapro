//
//  CategoryTechnologiesVC.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/27/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class CategoryTechnologiesVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CategoryDataService.instance.getCategories().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTechnologiesCell") as? CategoryTechnologiesCell {
            let category = CategoryDataService.instance.getCategories()[indexPath.row]
            cell.configureCell(category: category)
            return cell
        } else {
            return CategoryTechnologiesCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = CategoryDataService.instance.getCategories()[indexPath.row]
        performSegue(withIdentifier: "TechnologiesVC", sender: category)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let technologiesVC = segue.destination as? TechnologiesVC {

            assert(sender as? CategoryTechnologies != nil)
            technologiesVC.initTech(category: sender as! CategoryTechnologies)
        }
    }
}
