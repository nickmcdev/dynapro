//
//  TestimonialVC.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/17/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit
import FirebaseCore
import FirebaseDatabase
import SDWebImage

class TestimonialVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var testimonialArray = [Testimonial]()
    var id = ""
    
    
    // Outlets
    @IBOutlet weak var videoTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        videoTableView.delegate = self
        videoTableView.dataSource = self
        
        TestimonialsDataService.instance.getYouTubeData { (returnedTestimonialArray) in
            self.testimonialArray = returnedTestimonialArray
            self.videoTableView.reloadData()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        TestimonialsDataService.instance.getYouTubeData { (returnedTestimonialArray) in
            self.testimonialArray = returnedTestimonialArray
            //self.videoTableView.reloadData()
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testimonialArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "testCell", for: indexPath) as? TestimonialCell {
            let testimonial = testimonialArray[indexPath.row]
            let url = URL(string: testimonial.imageName)
            //id = testimonial.id
            cell.videoLabel.text = testimonial.title
            cell.videoImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder.png"), options: [.progressiveDownload, .continueInBackground])
            
            return cell
        } else {
            return TestimonialCell()
        }
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let height = view.frame.width * 9/16
//        return height
//    }
//    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 300
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "videoView", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "videoView" {
            if let indexPath = self.videoTableView.indexPathForSelectedRow {
                let videoVC = segue.destination as! VideoVC
                let testimonialSeg = testimonialArray[indexPath.row]
                videoVC.videoID = testimonialSeg.id
            }
        }
    }
    
}
