//
//  SplashScreenVC.swift
//  dynaPro
//
//  Created by Nick McWherter on 12/22/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit

class SplashScreenVC: UIViewController {
    
    var dynaLogoImageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dynaLogoImageView = UIImageView()
        dynaLogoImageView = UIImageView(frame: CGRect(x: (self.view.bounds.width/2) - 75, y: (self.view.bounds.height/2) - 75, width: 150, height: 150))
        
        dynaLogoImageView?.contentMode = .scaleAspectFit
        
        dynaLogoImageView!.image = UIImage(named: "dynaSplash.png")
        self.view.addSubview(dynaLogoImageView!)
        
        animate()
        
        self.view.backgroundColor = UIColor(red: 14/255.0, green: 118/255.0, blue: 210/255.0, alpha: 1)
        //self.view.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1)

    }

    func animate() {
        
        let duration = 1.0
        let delay = 1.5
        
        UIView.animateKeyframes(withDuration: duration, delay: delay, options: [], animations: {
            
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.33, animations: {
                self.dynaLogoImageView!.bounds = CGRect(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2, width: 75, height: 75)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.33, animations: {
                self.dynaLogoImageView!.bounds = CGRect(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2, width: 1500, height: 1500)
            })
            UIView.addKeyframe(withRelativeStartTime: 0.8, relativeDuration: 0.33, animations: {
                self.dynaLogoImageView!.bounds = CGRect(x: self.view.frame.size.width/2, y: self.view.frame.size.height/2, width: 3000, height: 3000)
            })
            
        }, completion: { (finished) in
            self.performSegue(withIdentifier: "toApp", sender: self)
            
        })
    }

}
