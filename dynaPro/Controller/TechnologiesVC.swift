//
//  TechnologiesVC.swift
//  dynaPro
//
//  Created by Nick McWherter on 11/18/17.
//  Copyright © 2017 Nick McWherter. All rights reserved.
//

import UIKit
import SDWebImage

class TechnologiesVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // Outlets
    @IBOutlet weak var collectionView: UICollectionView!
   
    
    var technologiesArray = [Technologies]()
    var techName = ""
    
    // app
    let appImages = CategoryDataService.instance.getAppImages()
    let appTitles = CategoryDataService.instance.getAppTitles()
    
    // devops
    let devopsImages = CategoryDataService.instance.getDevopsImages()
    let devopsTitles = CategoryDataService.instance.getDevopsTitles()
    
    // web
    let webImages = CategoryDataService.instance.getWebImages()
    let webTitles = CategoryDataService.instance.getWebTitles()
    
    // aws
    let awsImages = CategoryDataService.instance.getAWSImages()
    let awsTitles = CategoryDataService.instance.getAWSTitles()
    
    // cloud
    let cloudImages = CategoryDataService.instance.getCloudImages()
    let cloudTitles = CategoryDataService.instance.getCloudTitles()
    
    // database
    let dataImages = CategoryDataService.instance.getDataImages()
    let dataTitles = CategoryDataService.instance.getDataTitles()
    
    // enterprise
    let enterImages = CategoryDataService.instance.getEnterImages()
    let enterTitles = CategoryDataService.instance.getEnterTitles()
    
    // integration
    let integImages = CategoryDataService.instance.getIntegImages()
    let integTitles = CategoryDataService.instance.getIntegTitles()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self

        // Remove the "Back" text on Navigation Bar
        self.navigationController?.navigationBar.topItem?.title = " "
    }

    func initTech(category: CategoryTechnologies) {
        technologiesArray = CategoryDataService.instance.getTech(forCategoryTitle: category.title)
        navigationItem.title = category.title
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return technologiesArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "techCell", for: indexPath) as? TechnologiesCell {
            if navigationItem.title == "Application performance" {
                cell.techImageView.sd_setImage(with: URL(string: appImages[indexPath.row]))
                cell.techLabel.text = appTitles[indexPath.row]
                } else if navigationItem.title == "DevOps & CI/CD" {
                cell.techImageView.sd_setImage(with: URL(string: devopsImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "techPlaceholder"), options: [.continueInBackground, .progressiveDownload])
                    cell.techLabel.text = devopsTitles[indexPath.row]
                } else if navigationItem.title == "Web technologies" {
                    cell.techImageView.sd_setImage(with: URL(string: webImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "techPlaceholder"), options: [.continueInBackground, .progressiveDownload])
                    cell.techLabel.text = webTitles[indexPath.row]
                } else if navigationItem.title == "AWS technologies" {
                    cell.techImageView.sd_setImage(with: URL(string: awsImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "techPlaceholder"), options: [.continueInBackground, .progressiveDownload])
                    cell.techLabel.text = awsTitles[indexPath.row]
                } else if navigationItem.title == "Cloud & infrastructure" {
                    cell.techImageView.sd_setImage(with: URL(string: cloudImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "techPlaceholder"), options: [.continueInBackground, .progressiveDownload])
                    cell.techLabel.text = cloudTitles[indexPath.row]
                } else if navigationItem.title == "Database technologies" {
                    cell.techImageView.sd_setImage(with: URL(string: dataImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "techPlaceholder"), options: [.continueInBackground, .progressiveDownload])
                    cell.techLabel.text = dataTitles[indexPath.row]
                } else if navigationItem.title == "Enterprise technologies" {
                    cell.techImageView.sd_setImage(with: URL(string: enterImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "techPlaceholder"), options: [.continueInBackground, .progressiveDownload])
                    cell.techLabel.text = enterTitles[indexPath.row]
                } else {
                    cell.techImageView.sd_setImage(with: URL(string: integImages[indexPath.row]), placeholderImage: #imageLiteral(resourceName: "techPlaceholder"), options: [.continueInBackground, .progressiveDownload])
                    cell.techLabel.text = integTitles[indexPath.row]
                    }
                    return cell
                }
            return TechnologiesCell()
        }
    
}
